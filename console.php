<?php

/**
 * Console logic here
 */
require_once('vendor/autoload.php');

use App\Route\ApplicationRoute;

$route = new ApplicationRoute();

$route->setToConsoleApp(true);

$route->setArguments($argv);


$route->handle();

if (!empty($route->getFirstError())) {

    echo $route->getFirstError();
}
