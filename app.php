<?php

/**
 * Application logic here
 */
require_once('vendor/autoload.php');

use App\Route\ApplicationRoute;

$route = new ApplicationRoute();

/* @var $requested_url string */
$requested_url = !empty($_REQUEST) ? $_REQUEST : false;

$route->setRequest($requested_url);

/**
 * @todo execute the appropriate controller and/or action , upload, validate the CSV with input data and render the results
 */
$route->handle();

if (!empty($route->getFirstError())) {

    echo $route->getFirstError();
}