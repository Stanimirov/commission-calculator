<?php

/**
 * Class ApplicationRoute - route the appropriate URL to controller(class) and action(method)
 * 
 * @todo make ApplicationRoute shared service
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Route;

use ReflectionClass;
use App\Common\Library\Text;
use App\Common\Constants;

class ApplicationRoute {

    /**
     * @var string
     */
    private $url = '_url';

    /**
     * @var string Default controller(class) name to execute
     */
    private $default_controller = 'index';

    /**
     * @var string Default action(method) name to execute
     */
    private $default_action = 'index';

    /**
     * @var string Controller Name
     */
    private $controller;

    /**
     * @var string Controller Name Ext.
     */
    private $controller_ext = 'Controller';

    /**
     * @var string Action (method name)
     */
    private $action;

    /**
     * @var string Action name Ext.
     */
    private $action_ext = 'Action';

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var array
     */
    private $request = [];

    /**
     * @var array
     */
    private $arguments = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @var bool
     */
    private $is_console_app = false;

    /**
     * Routing logic when executing WEB application
     * 
     * @param string $request $_REQUEST request
     */
    public function setRequest($request = []) {

        if (!empty($request[$this->url])) {

            $parameters = !empty($request[$this->url]) ? explode('/', $request[$this->url]) : [];

            $this->parameters = array_values(array_filter($parameters));

            unset($request[$this->url]);
        }

        $this->request = $request;
    }

    /**
     * @param string $key
     * 
     * @return array|mixed Query request or key value if parameter is set
     */
    public function getRequest(string $key = '') {

        if (!empty($key)) {

            return array_key_exists($key, $this->request) ? $this->request[$key] : false;
        }

        return $this->request;
    }

    /**
     * Routing logic when executing the console application
     * 
     * @param array $arguments
     * @param int $args number of arguments
     */
    public function setArguments($arguments = []) {

        //remove the script name from arguments and reset the array
        if (!empty($arguments)) {

            unset($arguments[0]);
        }

        $this->parameters = array_values(array_filter($arguments));

        //remove controller from arguments
        if (!empty($arguments[1])) {

            unset($arguments[1]);
        }

        //remove action from arguments
        if (!empty($arguments[2])) {

            unset($arguments[2]);
        }

        $this->arguments = array_values(array_filter($arguments));
    }

    /**
     * @param int $index
     * 
     * @return array|mixed Returns the arguments or the value if key index is set
     */
    public function getArguments($index = false) {

        if (is_int($index) || $index !== false) {

            return array_key_exists($index, $this->arguments) ? $this->arguments[$index] : false;
        }

        return $this->arguments;
    }

    /**
     * Sets variable to indicate that console application will be used
     * 
     * @param bool $console_app
     */
    public function setToConsoleApp($console_app = false) {

        $this->is_console_app = $console_app === true ? true : false;
    }

    /**
     * Check whenever that console application is used
     * 
     * @return bool
     */
    public function isConsoleApp() {

        return $this->is_console_app;
    }

    /**
     * @return array list with application parameters
     */
    public function getParameters() {

        return $this->parameters;
    }

    /**
     * @return string NAMESPACE 
     */
    public function getApplicationNameSpace() {

        return $this->isConsoleApp() ? '\\App\\Console\\Controllers' : '\\App\\Web\\Controllers';
    }

    /**
     * Convert first parameter to camel case string
     * 
     * @return string Controller to execute
     */
    public function getControllerName() {

        if (!empty($this->controller)) {
            return $this->controller;
        }

        $params = $this->getParameters();

        if (empty($params[0])) {

            return ucfirst($this->default_controller) . $this->controller_ext;
        }

        $this->controller = Text::camelize($params[0]);

        return $this->controller . $this->controller_ext;
    }

    /**
     * @return string Action to execute
     */
    public function getActionName() {

        if (!empty($this->action)) {
            return $this->action;
        }

        $params = $this->getParameters();

        if (empty($params[1])) {

            return $this->default_action . $this->action_ext;
        }

        $action = Text::camelize($params[1]);

        $this->action = lcfirst($action) . $this->action_ext;

        return $this->action;
    }

    /**
     * Run application
     * 
     * @return mixed The method result
     */
    public function handle() {

        $controller = $this->getApplicationNameSpace() . '\\' . $this->getControllerName();

        try {

            $class = new ReflectionClass($controller);
        } catch (\ReflectionException $exc) {

            $this->setError($exc->getMessage());

            return false;
        }

        //checks if action in controller exists
        if (!$class->hasMethod($this->getActionName())) {

            $this->setError(Constants\ApplicationRouteConstants::ACTION_NOT_EXISTS);

            return false;
        }

        /**
         * @todo remove controller and action from parameters
         */
        $method = $class->getMethod($this->getActionName());

        //pass appliccation route to BaseController
        $obj = $class->newInstance($this);

        return $method->invoke($obj);
    }

    /**
     * @return array List with error messages
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * @return string|bool First error or FALSE if no errors
     */
    public function getFirstError() {

        return !empty($this->errors[0]) ? $this->errors[0] : false;
    }

    /**
     * Add error message to list with error messages
     * 
     * @param string $message
     */
    private function setError($message = false) {

        if (empty($message)) {
            return false;
        }

        $this->errors[] = $message;
    }

}
