<?php

namespace App\Common\Constants;

/**
 * class InputDataTypes
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class InputDataTypes {

    /**
     * @var string
     */
    const USER_TYPE_NATURAL = 'natural';

    /**
     * @var string
     */
    const USER_TYPE_LEGAL = 'legal';

    /**
     * @var string
     */
    const OP_TYPE_CASH_IN = 'cash_in';

    /**
     * @var string
     */
    const OP_TYPE_CASH_OUT = 'cash_out';

}
