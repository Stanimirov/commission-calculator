<?php

namespace App\Common\Constants;

/**
 * class MessagesConstants
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class MessagesConstants {

    /**
     * @var string
     */
    const FILE_INPUT_NOT_EXISTS = 'Input data file does not exists';

    /**
     * @var string
     */
    const INPUT_DATA_EMPTY = 'Not enough data';

}
