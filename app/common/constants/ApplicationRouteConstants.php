<?php

/**
 * class ApplicationRouteConstants
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Common\Constants;

class ApplicationRouteConstants {

    /**
     * @var string
     */
    const ACTION_NOT_EXISTS = 'Requested Action does not exists';

    /**
     * @var string
     */
    const CONTROLLER_NOT_EXISTS = 'Requested Controller does not exists';

}
