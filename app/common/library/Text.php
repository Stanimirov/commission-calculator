<?php

/**
 * class Text
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Common\Library;

class Text {

    /**
     * Camelize text
     * 
     * @param string $string Text to camelize
     * @param string $delimeter dash "-" is the default
     * 
     * @return string CamelizedText
     *      
     */
    public static function camelize($string, $delimeter = '-') {

        $parts = explode($delimeter, $string);

        $camelize = array_map('ucfirst', $parts);

        return implode('', $camelize);
    }

}
