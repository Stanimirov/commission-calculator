<?php

/**
 * Class of BaseController. 
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Common\Controllers;

use App\Route\ApplicationRoute;

class BaseController {

    /**
     * @var \App\Route\ApplicationRoute
     */
    var $route;

    /**
     * @param App\Route\ApplicationRoute $route
     */
    public function __construct(ApplicationRoute $route) {

        $this->route = $route;
    }

}
