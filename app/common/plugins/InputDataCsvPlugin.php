<?php

namespace App\Common\Plugins;

/**
 * class InputDataCsvPlugin Reads the input data CSV file
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class InputDataCsvPlugin {

    /**
     * @var string Input data file name
     */
    protected $file;

    /**
     * @var string default path to input data files
     */
    protected $file_path = 'data';

    /**
     * @var array CSV data
     */
    protected $items = [];

    /**
     * @var array CSV header keys
     */
    protected $header = [];

    /**
     * 
     */
    public function __construct() {
        
    }

    /**
     * Sets the path from where to pickup files
     * 
     * @param strin $file_path
     */
    public function setPath($file_path) {

        $this->file_path = $file_path;
    }

    /**
     * @return string path to file
     */
    public function getPath() {

        // just be sure that a slash at the end of path is not added
        return chop($this->file_path, '/') . '/';
    }

    /**
     * @return array CSV Header keys
     */
    public function getHeaderKeys() {

        return $this->header;
    }

    /**
     * Sets the file name to work with 
     * 
     * @param strin $file_name
     */
    public function setFile($file_name) {

        $this->file = $file_name;
    }

    /**
     * @return string path to file to read
     * @return bool false if file does not exists
     */
    public function getFile() {

        return !empty($this->file) && file_exists($this->getPath() . $this->file) ? $this->getPath() . $this->file : false;
    }

    /**
     * Read uploaded CSV and validate for errors
     * 
     * @return true If file exists and has been read
     */
    public function parseCsv() {

        //assgn to variable to prevent unused checks
        $file = $this->getFile();

        if (empty($file)) {

            return false;
        }

        //read file
        $fn = \fopen($file, 'r');

        $line = 0;

        $row_error = '';

        while (!feof($fn)) {

            //get the header keys from first line
            ++$line;
            if ($line === 1) {
                $this->header = \str_getcsv(\fgets($fn));
                continue;
            }

            /**
             * @todo add delimeter and enclosure in CONF file
             */
            $result = \str_getcsv(\fgets($fn));

            /**
             * @todo make additional validation
             */
            if (count($result) !== count($this->header)) {
                $row_error .= $line . ',';
                continue;
            }

            $this->items[] = array_combine($this->header, $result);
        }

        if (!\is_resource($fn)) {

            return false;
        }

        return \fclose($fn);
    }

    /**
     * @return array Input data ready for calculations
     */
    public function getInputData() {

        return $this->items;
    }

}
