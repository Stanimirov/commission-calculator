<?php

namespace App\Common\Plugins;

/**
 * class of CalculateCommissionsPlugin
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class CalculateCommissionsPlugin {

    protected $allowed_currencies = ['EUR', 'USD', 'JPY'];

    public function __construct() {
        
    }

    /**
     * @return array Currencies allowed
     */
    public function getAllowedCurrencies() {

        return $this->allowed_currencies;
    }

}
