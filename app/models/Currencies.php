<?php

/**
 * Class of Currencies.
 * 
 * @todo integrate with ORM lib
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Models;

class Currencies {

    protected static $currencies = ['EUR', 'USD', 'JPY'];

    /**
     * @return array List with supported currencies
     */
    public static function find() {

        return self::$currencies;
    }

}
