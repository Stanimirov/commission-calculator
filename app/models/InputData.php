<?php

/**
 * Class of InputData represent input file CSV columns, columns mapping and db columns.
 * 
 * @todo integrate with ORM lib
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Models;

class InputData {

    /**
     *
     * @var string
     * @Column(column="operation_date", type="date", nullable=true)
     */
    protected $operation_date;

    /**
     *
     * @var int
     * @Column(column="user_id", type="int", nullable=false)
     */
    protected $user_id;

    /**
     *
     * @var string
     * @Column(column="user_type", type="string", length="50" nullable=false)
     */
    protected $user_type;

    /**
     *
     * @var string
     * @Column(column="operation_type", type="string", length="50" nullable=false)
     */
    protected $operation_type;

    /**
     *
     * @var float
     * @Column(column="operation_amount", type="float", length="" nullable=false)
     */
    protected $operation_amount;

    /**
     *
     * @var string
     * @Column(column="operation_currency", type="string", length="50" nullable=false)
     */
    protected $operation_currency;

    public function __construct() {
        
    }

    /**
     * @param string $operation_date Operation date in format Y-m-d
     */
    public function setOperationDate($operation_date) {

        $this->operation_date = $operation_date;

        return $this;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id) {

        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @param string $user_type
     */
    public function setUserType($user_type) {

        $this->user_type = $user_type;

        return $this;
    }

    /**
     * @param string $operation_type
     */
    public function setOperationType($operation_type) {

        $this->operation_type = $operation_type;

        return $this;
    }

    /**
     * @param float $operation_amount
     */
    public function setOperationAmount($operation_amount) {

        $this->operation_amount = $operation_amount;

        return $this;
    }

    /**
     * @param string $operation_currency
     */
    public function setOperationCurrency($operation_currency) {

        $this->operation_currency = $operation_currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperationDate() {

        return $this->operation_date;
    }

    /**
     * @return int
     */
    public function getUserId() {

        return $this->user_id;
    }

    /**
     * @return string
     */
    public function getUserType() {

        return $this->user_type;
    }

    /**
     * @return string
     */
    public function getOperationType() {

        return $this->operation_type;
    }

    /**
     * @return float
     */
    public function getOperationAmount() {

        return $this->operation_amount;
    }

    /**
     * @return string
     */
    public function getOperationCurrency() {

        return $this->operation_currency;
    }

    /**
     * Assign data to model.
     * If mapping array is set, keys are the user defined columns and the values are real db column names 
     * 
     * @param array $data input data (key => value pair)
     * @param array $mapping key mapping eg. [operation_date => custom_array_key]
     */
    public function assign($data = [], $mapping = []) {

        //model columns or user mapping 
        $columns = !empty($mapping) ? $mapping : $this->columnMap();

        foreach ($data as $key => $value) {

            $column = array_key_exists($key, $columns) ? $columns[$key] : false;

            //be sure there is a column defined and class property exists
            if (empty($column) || !property_exists(get_class(), $column)) {
                continue;
            }

            //assign value to class property
            $this->{$column} = $value;
        }
    }

    public function columnMap() {
        return [
            'operation_date' => 'operation_date',
            'user_id' => 'user_id',
            'user_type' => 'user_type',
            'operation_type' => 'operation_type',
            'operation_amount' => 'operation_amount',
            'operation_currency' => 'operation_currency'
        ];
    }

}
