<?php

/**
 * Class CalculateCommissionsController 
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */

namespace App\Console\Controllers;

use App\Common\Controllers\BaseController;
use App\Common\Constants\MessagesConstants;
use App\Common\Plugins\InputDataCsvPlugin;
use App\Models\InputData;

class CalculateCommissionsController extends BaseController {

    public function indexAction() {
        
    }

    /**
     * Reads and process the Input Data CSV file by steps
     */
    public function inputAction() {

        $file = $this->route->getArguments(0);

        //load plugin
        $input_data = new InputDataCsvPlugin();

        //set file name
        $input_data->setFile($file);

        /**
         * @todo output result to STDOUT
         */
        if (empty($input_data->getFile())) {

            echo MessagesConstants::FILE_INPUT_NOT_EXISTS;

            return false;
        }

        //parse file
        $input_data->parseCsv();

        /**
         * @todo output result to STDOUT
         */
        if (empty($input_data->getInputData())) {

            echo MessagesConstants::INPUT_DATA_EMPTY;

            return false;
        }

        //iterate the parsed data and map to model, then easy calculate and/or validate using getters 
        foreach ($input_data->getInputData() as $data) {

            /* @var $model \App\Models\InputData */
            $model = new InputData();

            /**
             * assign values to model
             * @todo can be done mapping with different CSV header keys
             */
            $model->assign($data);

            //call plugin to calculate the commissions 
            $model->getOperationType();

            $model->getOperationAmount();

            echo '<pre>';
            print_r($model);
            echo '</pre>';
        }

        //use the model and calculate
    }

    public function validateAction() {
        
    }

}
